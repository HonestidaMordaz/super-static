'use strict'

const superStatic = require('../index')

const http = require('http')
const path = require('path')

superStatic.serve(__dirname, {
	index: 'index.html'
})

http.createServer(function (req, res) {
		superStatic.process(req, res)
}).listen(8080)

console.log('Server running at port 8080')