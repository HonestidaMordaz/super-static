'use strict'

const path = require('path')
const url = require('url')
const fs = require('fs')

function superStatic () {
}

superStatic.prototype.serve = function (directory, options) {
	if (typeof(directory) === 'object') {
		this._directory = '/'
		this._options = directory
	} else {
		this._directory = directory
		this._options = options
	}

	this._indexFile = this._options.index || {}
	this._errorFile = this._options.error || {}
}

superStatic.prototype.process = function (req, res) {
	console.log(`${req.method} ${req.url}`)

	let file = url.parse(req.url)
	let pathFile
	let buffer

	if (file.path === '/')
		this._file = this._indexFile
	else
		this._file = file.path.slice(1)

	try {
		pathFile = path.join(this._directory, this._file)
		buffer = fs.readFileSync(pathFile)
	} catch (e) {
		buffer = null
	}

	if (!buffer) {
		this._statusCode = 500
		this._statusMessage = 'Internal Error'
		this._contentType = { 'content-type' : 'text/plain' }

		if (typeof(this._errorFile) != 'object')
			buffer = fs.readFileSync(this._errorFile)
		else
			buffer = '404 Not Found'

	} else {
		this._statusCode = 200
		this._statusMessage = 'OK'
		this._contentType = { 'content-type' : 'text/plain' }
	}

	res.write(buffer.toString())
	res.end()
}

module.exports = exports = new superStatic()