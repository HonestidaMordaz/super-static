# Super Static

Serve static files

## Ussage

	var superStatic = require('super-static')
	var http = require('http')

	superStatic.serve(__dirname + 'public', {
		index: 'index.html',
		error: 'error.html'
	})

	http.createServer(function (req, res) {
		superStatic.process(req, res)
	})